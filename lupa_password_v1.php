<?php 
	include 'config/config.php';
	// include 'plugins/phpmailer/PHPMailerAutoload.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Registrasi Akun Owner Sewa Menyewa</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Coiny" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Noticia+Text" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="assets/css/custom-login.css">

	<!-- <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script> -->
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<!-- <script src="plugins/jQuery/jquery-2.2.3.min.js"></script> -->
	<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.5.2.min.js"></script>

<body>
	<div class="container">
	<!-- <div id="particles"></div> -->

		<div class="box-login">
			<div class="font-style margin-bottom-20">
				<h2 class="text-center"><a href="index.php" style="text-decoration:none;">SEWO BRO</a></h2>
				<p class="text-center">LUPA PASSWORD DI SEWO BRO</p>
			</div>
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="box-login-grid">
						<div class="box-title-login">
							<h3 class="text-center"><b>LUPA PASSWORD</b></h3>
						</div>
						<?php
						if (isset($_POST['lupa_password']))
						{
							$hasilowner = $akun_owner->reset_owner($_POST['email']);

							if ($hasilowner=="berhasil")
							{
    							echo "<div class='alert alert-success'><strong>SUKSES!</strong> Password owner telah berhasil dikirim, silahkan dicek email anda. Terima kasih.</div>";
								echo "<meta http-equiv='refresh' content='2;url=login.php'>";
							}
							else
							{
								$hasilmember = $akun_member->reset_member($_POST['email']);
								if ($hasilmember=="berhasil")
								{
									echo "<div class='alert alert-success'><strong>SUKSES!</strong> Password member telah berhasil dikirim, silahkan dicek email anda. Terima kasih.</div>";
									echo "<meta http-equiv='refresh' content='2;url=login.php'>";
								}
								else
								{
									echo "<div class='alert alert-danger'>
									<strong>GAGAL!</strong> Password gagal dikirim, silahkan dicek kembali. Terima kasih.
									</div>";
									echo "<meta http-equiv='refresh' content='2;url=lupa_password.php'>";
								}
							}
						}
						?>
						<form method="POST">
							<div class="form-group">
								<label>Email</label>
								<input type="text" name="email" class="form-control" placeholder="Input Email">
							</div>
							
							<div class="form-group" required>
								<label>Captcha</label>
								<div class="g-recaptcha" data-sitekey="6Lf5ek0UAAAAAImupyDridMfOL3BjPtQ9Vw1jfuL"></div>
							</div>
							<div class="form-group">
								<!-- <input type="submit" name="registrasi_owner" id="register" class="btn btn-success form-control" value="SIGN UP"> -->
								<button type="submit" name="lupa_password" id="register" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; SEND</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>


<!--Function owner-->
function reset_owner ($email)
	{
		$ambil = $this->koneksi->query("SELECT * FROM owner WHERE email_owner='$email'");
        $hasil = $ambil->num_rows;
        if ($hasil==1) 
        {
            // nis akan dicek terlebh dahulu, jika ada maka berhasil disimpan
            // membuat karakter
            $karakter = "qwertyuiopasdfghjklzxcvbnm1234567890";
            $string = '';
            for ($i=0; $i < 8 ; $i++) 
            { 
                $post = rand(0, strlen($karakter)-1);
                $string .= $karakter[$post];
            }
            $pesan['email_owner']=$email;
            $pesan['password_owner']=$string;

            $subject = "Reset Password";
            $message = 
            "
            <html>
            <head>
                <title>Lupa Password Owner</title>
            </head>
            <body>
                <p>selamat anda telah mereset password</p>
                <p>Email : {email_owner}</p>
                <p>password : {password_owner}</p>
            </body>
            </html>
            ";

            foreach ($pesan as $key => $value) 
            {
                $message = str_replace("{".$key."}", $value, $message);
            }

            $headers = "MIME-Version: 1.0"."\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8"."\r\n";

            $headers .= 'from : <sewamenyewajogja@gmail.com>';

            mail($email, $subject, $message, $headers);
            $password = sha1($string);
            $this->koneksi->query("UPDATE owner SET password_owner='$password' WHERE email_owner='$email'");

            // jika nis ada di database maka berhasil disimpan
            return 'berhasil';
        }
        else
        {
            // jika nis tidak sesuai dengan database maka gagal disimpan
            return 'gagal';
        }
	}
	
	

<!--Function Member-->
function reset_member ($email)
	{
		$ambil = $this->koneksi->query("SELECT * FROM member WHERE email_member='$email'");
        $hasil = $ambil->num_rows;
        if ($hasil==1) 
        {
            // nis akan dicek terlebh dahulu, jika ada maka berhasil disimpan
            // membuat karakter
            $karakter = "qwertyuiopasdfghjklzxcvbnm1234567890";
            $string = '';
            for ($i=0; $i < 8 ; $i++) 
            { 
                $post = rand(0, strlen($karakter)-1);
                $string .= $karakter[$post];
            }
            $pesan['email_member']=$email;
            $pesan['password_member']=$string;

            $subject = "Reset Password";
            $message = 
            "
            <html>
            <head>
                <title>Lupa Password Member</title>
            </head>
            <body>
                <p>selamat anda telah mereset password</p>
                <p>Email : {email_member}</p>
                <p>password : {password_member}</p>
            </body>
            </html>
            ";

            foreach ($pesan as $key => $value) 
            {
                $message = str_replace("{".$key."}", $value, $message);
            }

            $headers = "MIME-Version: 1.0"."\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8"."\r\n";

            $headers .= 'from : <sewamenyewajogja@gmail.com>';

            mail($email, $subject, $message, $headers);
            $password = sha1($string);
            $this->koneksi->query("UPDATE member SET password_member='$password' WHERE email_member='$email'");

            // jika nis ada di database maka berhasil disimpan
            return 'berhasil';
        }
        else
        {
            // jika nis tidak sesuai dengan database maka gagal disimpan
            return 'gagal';
        }
	}